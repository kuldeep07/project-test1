package com.example.kuldeep.projecttest1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(MainActivity.this, "Main Activity created", Toast.LENGTH_SHORT).show();
        Log.i("onCreate():","Main Activity created");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "Main activity started", Toast.LENGTH_SHORT).show();
        Log.i("onStart():","Main Activity started");
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Toast.makeText(this, "Main Activity Restarted", Toast.LENGTH_SHORT).show();
        Log.i("onRestart():","Main Activity Restated");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "Main Activity Resumed", Toast.LENGTH_SHORT).show();
        Log.i("onResume():","Main Activity Resumed");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "Main Activity Paused", Toast.LENGTH_SHORT).show();
        Log.i("onPause():","Main Activity Paused");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, "Main Activity Stopped", Toast.LENGTH_SHORT).show();
        Log.i("onStop():","Main Activity Stopped");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Main Activity Destroyed", Toast.LENGTH_SHORT).show();
        Log.i("onDestroy():","Main Activity Destroyed");
    }


}
